from .effect import Effect2, Effect3
from mud.events import PayWithEvent, PayEvent

class PayEffect(Effect2):
    EVENT = PayEvent

class PayWithEffect(Effect3):
    EVENT = PayWithEvent
